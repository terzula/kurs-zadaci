﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] brojevi = {0,1,2,3,4,5,6};
            List<int> intList = new List<int> { 1, 2, 3, 4, 5 };

            var upit = from broj in brojevi
                       where broj % 2 == 0
                       orderby broj ascending
                       select broj;


            var upitLista = intList.Where(x => x % 2 == 0).Select(x => x);
            var upitNiz = brojevi.Where(x => x % 2 == 0).Select(x => x);


            foreach (int i in upit)
            {
                Console.WriteLine(i + "");
            }

        }
    }
}
