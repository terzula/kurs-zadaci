﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proizvod
{
    class Program
    {
        static void Main(string[] args)
        {
            //    List<Proizvod> listaProizvoda = new List<Proizvod>
            //    {
            //        new Proizvod
            //        {
            //            ProizvodId = 1,
            //            NazivProizvoda = "Cips",
            //            Kategorija = "Grickalice",
            //            JedinicnaCena = 50,
            //            JedinicaNaLageru = 15
            //        },
            //        new Proizvod
            //        {
            //        ProizvodId = 2,
            //        NazivProizvoda = "Smoki",
            //        Kategorija = "Grickalice",
            //        JedinicnaCena = 30,
            //        JedinicaNaLageru = 20
            //        },
            //        new Proizvod
            //        {
            //        ProizvodId = 3,
            //        NazivProizvoda = "Grisini",
            //        Kategorija = "Grickalice",
            //        JedinicnaCena = 45,
            //        JedinicaNaLageru = 12
            //        }
            //};

            //Prave se na isti nacin samo u klasi.
            //Proizvod p1 = new Proizvod
            //{
            //    ProizvodId = 1,
            //    NazivProizvoda = "Cips",
            //    Kategorija = "Grickalice",
            //    JedinicnaCena = 50,
            //    JedinicaNaLageru = 15
            //};

            //Proizvod p2 = new Proizvod
            //{
            //    ProizvodId = 2,
            //    NazivProizvoda = "Smoki",
            //    Kategorija = "Grickalice",
            //    JedinicnaCena = 30,
            //    JedinicaNaLageru = 20
            //};

            //Proizvod p3 = new Proizvod
            //{
            //    ProizvodId = 3,
            //    NazivProizvoda = "Grisini",
            //    Kategorija = "Grickalice",
            //    JedinicnaCena = 45,
            //    JedinicaNaLageru = 12
            //};

            //listaProizvoda.Add(p1);
            //listaProizvoda.Add(p2);
            //listaProizvoda.Add(p3);

            List<Proizvod> listaProizvoda = new List<Proizvod>();
            Proizvod onoStoCeDaMiVratiFunkcija = new Proizvod();


            listaProizvoda = onoStoCeDaMiVratiFunkcija.GetListaProizvoda();


            //Fluentni linq
            var upit = from proizvod in listaProizvoda
                       where proizvod.JedinicaNaLageru > 15
                       select proizvod;
            
            //Primer sa lambda funkcijama
            var rezultatUpita = listaProizvoda.Where(x => x.JedinicaNaLageru > 15);

            //listaProizvoda.Where(x => { 
            //        //Ovde ide telo funkcije.
            //});

            foreach (Proizvod p in rezultatUpita)
            {
                Console.WriteLine(p.ProizvodId);
            }
        }
    }
}
